import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './login/login.component';

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', loadChildren: 'app/home/home.module#HomeModule', canLoad: [AuthGuard] }
];

export const routing = RouterModule.forRoot(appRoutes);