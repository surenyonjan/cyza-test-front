import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad } from '@angular/router';

import { apiUtils } from '../utils/api-utils';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private router: Router) { }

  canActivate(): boolean {

    if (apiUtils.getCurrentUser()) {

      return true
    }

    this.router.navigate(['/login']);
    return false
  }

  canLoad(): boolean {
    return this.canActivate()
  }
}