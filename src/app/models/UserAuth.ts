/**
 * User Authentication Data
 */
export class UserAuth {
  accessToken: string;
  expiresIn: number;
  refreshToken: string;

  constructor(a: string, e: number, r: string) {
    this.accessToken = a;
    this.expiresIn = e;
    this.refreshToken = r;
  }
}