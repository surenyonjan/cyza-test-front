define(["require", "exports"], function (require, exports) {
    /**
     * User Authentication Data
     */
    var UserAuth = (function () {
        function UserAuth(a, e, r) {
            this.accessToken = a;
            this.expiresIn = e;
            this.refreshToken = r;
        }
        return UserAuth;
    })();
    exports.UserAuth = UserAuth;
});
//# sourceMappingURL=UserAuth.js.map