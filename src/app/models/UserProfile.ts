/**
 * User Profile details
 */
export class UserProfile {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  website: string;
  address: {
    city: string;
    state: string;
    zip: string;
  };
  phone: string;
  stars: number;
  reviewsCount: number;
  followersCount: number;

  constructor(obj: any) {
    Object.assign(this, obj)
  }
}