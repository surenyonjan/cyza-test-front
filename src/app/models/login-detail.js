define(["require", "exports"], function (require, exports) {
    /**
     * Login Detail
     */
    var LoginDetail = (function () {
        function LoginDetail(e, p) {
            this.email = e;
            this.password = p;
        }
        return LoginDetail;
    })();
    exports.LoginDetail = LoginDetail;
});
//# sourceMappingURL=login-detail.js.map