define(["require", "exports"], function (require, exports) {
    /**
     * User Profile details
     */
    var UserProfile = (function () {
        function UserProfile(obj) {
            Object.assign(this, obj);
        }
        return UserProfile;
    })();
    exports.UserProfile = UserProfile;
});
//# sourceMappingURL=UserProfile.js.map