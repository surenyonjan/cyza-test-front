import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import '../rxjs-operators';

import { LoginService } from '../services/login.service';
import { UserService } from '../services/user.service';
import { UserProfile } from '../models/UserProfile';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  profile: UserProfile;
  maxStars: number = 5;

  constructor(
    private loginService: LoginService,
    private userService: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getProfile().subscribe(userProfile => this.profile = userProfile, err => console.log(err))
  }

  logout() {
    this.loginService.logout();
    this.router.navigate(['/login'])
  }

  filledStars() {
    return Array(this.profile.stars).fill(this.profile.stars)
  }

  blankStars() {
    return Array(this.maxStars - this.profile.stars).fill(this.maxStars)
  }
}
