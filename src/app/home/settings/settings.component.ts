import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';
import '../../rxjs-operators';

import { UserService } from '../../services/user.service';
import { WithFormErrorHandler } from '../../utils/form-handler';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']

})
export class SettingsComponent implements OnInit, WithFormErrorHandler {

  constructor(private userService: UserService, private location: Location) {
  }

  ngOnInit() {
  }

  changePassword(f: NgForm) {

    if (!this.isFormValid(f)) {
      return
    }

    this.unsetFormError();
    this.userService.changePassword(f.value.existingPassword, f.value.newPassword)
      .subscribe(resp => {
        console.log('Password changed!');
        this.location.back()
      }, err => {
        console.log(err);
        this.setFormError(err)
      })
  }

  isFormValid(f: NgForm): boolean {

    const fValues = f.value || {};
    if (fValues.newPassword != fValues.confirmPassword) {
      this.setFormError('Password confirmation do not match');
      return false
    }

    return true
  }

  goBack() {
    this.location.back()
  }

  // from mixin: WithFormErrorHandler
  hasFormError = false;
  formErrorMessage = '';
  setFormError: (msg: string) => void;
  unsetFormError: () => void;

}