import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { AboutComponent } from './about/about.component';
import { SettingsComponent } from './settings/settings.component';

import { HomeRoutingModule } from './home-routing.module';
import { AboutEditComponent } from './about/about-edit.component';
import { applyMixins } from '../utils/api-utils';
import { WithFormErrorHandler } from '../utils/form-handler';

applyMixins(SettingsComponent, [WithFormErrorHandler]);
applyMixins(AboutEditComponent, [WithFormErrorHandler]);


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    AboutComponent,
    SettingsComponent,
    AboutEditComponent
  ]
})
export class HomeModule {}