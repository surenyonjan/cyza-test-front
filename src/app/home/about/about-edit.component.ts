import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { NgForm } from '@angular/forms';

import '../../rxjs-operators';

import { UserService } from '../../services/user.service';
import { UserProfile } from '../../models/UserProfile';
import { WithFormErrorHandler } from '../../utils/form-handler';

@Component({
  selector: 'app-about-edit',
  templateUrl: './about-edit.component.html',
  styleUrls: ['./about-edit.component.css']
})
export class AboutEditComponent implements OnInit, WithFormErrorHandler {

  profile: UserProfile;

  constructor(private location: Location, private userService: UserService) { }

  ngOnInit() {
    this.userService.getProfile().subscribe(userProfile => this.profile = userProfile, err => console.log(err))
  }

  edit() {

    this.userService.editProfile(this.profile).subscribe(resp => {
      console.log('Profile edited!');
      this.location.back()
    }, errMsg => {
      console.log(errMsg);
      this.setFormError(errMsg)
    })
  }

  goBack() {
    this.location.back()
  }

  // from mixin: WithFormErrorHandler
  hasFormError = false;
  formErrorMessage = '';
  setFormError: (msg: string) => void;
  unsetFormError: () => void;

}
