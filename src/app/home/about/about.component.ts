import { Component, OnInit } from '@angular/core';

import '../../rxjs-operators';

import { UserService } from '../../services/user.service';
import { UserProfile } from '../../models/UserProfile';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  profile: UserProfile;

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getProfile().subscribe(userProfile => this.profile = userProfile, err => console.log(err))
  }

}
