import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { WithFormErrorHandler } from './utils/form-handler';
import { LoginService } from './services/login.service';
import { UserService } from './services/user.service';
import { AuthGuard } from './guards/auth.guard';
import { applyMixins } from './utils/api-utils';
import { routing } from './app.routing';

applyMixins(LoginComponent, [WithFormErrorHandler]);

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  declarations: [
    AppComponent,
    LoginComponent
  ],
  providers: [ LoginService, UserService, AuthGuard ],
  bootstrap: [AppComponent]
})
export class AppModule { }
