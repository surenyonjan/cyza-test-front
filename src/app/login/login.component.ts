import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import '../rxjs-operators';

import { LoginDetail } from '../models/login-detail';
import { LoginService } from '../services/login.service';
import { WithFormErrorHandler } from '../utils/form-handler';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, WithFormErrorHandler {

  loginDetail: LoginDetail;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    this.loginDetail = new LoginDetail('', '');
  }

  login() {
    this.loginService.login(this.loginDetail)
      .subscribe(resp => {
        console.log('Logged In!');
        this.router.navigate([''])
      }, errMsg => {
        console.log(errMsg);
        this.setFormError(errMsg)
      });
  }

  // from mixin: WithFormErrorHandler
  hasFormError = false;
  formErrorMessage = '';
  setFormError: (msg: string) => void;
  unsetFormError: () => void;
}
