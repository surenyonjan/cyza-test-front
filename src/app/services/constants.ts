import { Headers } from '@angular/http';

export const HEADER_JSON_TYPE = new Headers({'Content-Type': 'application/json'});

export function headerWithAuthToken(token: string): Headers {

  let _header = new Headers(HEADER_JSON_TYPE);

  _header.append('authorization', 'Bearer ' + token);

  return _header;
}