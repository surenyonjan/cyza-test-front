import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { LoginDetail } from '../models/login-detail';
import { HEADER_JSON_TYPE } from './constants';

import { apiUtils } from '../utils/api-utils';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {

  constructor(private http: Http) { }

  login(input: LoginDetail) {

    const request_url = '/api/v1/auth/token?grant_type=password&client_id=a823jkas87y3kjakjhsd&&client_secret=dksu287aokjfaouiusdia7127a5skd&username=' + input.email + '&password=' + input.password;
    return this.http.post(apiUtils.getApiUrl(request_url), {}, { headers: HEADER_JSON_TYPE })
      .map(resp => {

        // parse and then persist
        const current_user =  apiUtils.parseUserAuth(resp);
        localStorage.setItem('currentUser', JSON.stringify(current_user));
        return current_user
      })
      .catch(resp => {
        return Observable.throw(apiUtils.parseErrorMessage(resp))
      })
  }

  logout() {
    localStorage.removeItem('currentUser')
  }
}
