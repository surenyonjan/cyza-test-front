import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';

import { UserProfile } from '../models/UserProfile';
import { apiUtils } from '../utils/api-utils';
import { headerWithAuthToken } from './constants';


@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getProfile(): Observable<UserProfile> {
    return this.http.get(apiUtils.getApiUrl('/api/v1/profile'), {
        headers: headerWithAuthToken(apiUtils.getCurrentUser().accessToken)
      }).map(resp => {
        return apiUtils.parseUserProfile(resp)
      }).catch(resp => {
        return Observable.throw(apiUtils.parseErrorMessage(resp))
      })
  }

  changePassword(oldPassword: string, newPassword: string): Observable<Response> {
    return this.http.put(apiUtils.getApiUrl('/api/v1/profile/changePassword'),
      {
        'oldPassword': oldPassword,
        'newPassword': newPassword
      },
      {
        headers: headerWithAuthToken(apiUtils.getCurrentUser().accessToken)
      }).catch(resp => {
        return Observable.throw(apiUtils.parseErrorMessage(resp))
      })
  }

  editProfile(userProfile: UserProfile): Observable<Response> {
    return this.http.patch(apiUtils.getApiUrl('/api/v1/profile'),
      userProfile,
      {
        headers: headerWithAuthToken(apiUtils.getCurrentUser().accessToken)
      }).catch(resp => {
        return Observable.throw(apiUtils.parseErrorMessage(resp))
      })
  }
}