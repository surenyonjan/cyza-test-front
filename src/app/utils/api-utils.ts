import { Response } from '@angular/http';

import { UserAuth } from '../models/UserAuth';
import { UserProfile } from '../models/UserProfile';
import { environment } from '../../environments/environment'

class ApiUtils {

  host = environment.apiUrl;

  getApiUrl(url: string) {
    return this.host + url;
  }

  getCurrentUser(): UserAuth {
    return <UserAuth>JSON.parse(localStorage.getItem('currentUser'))
  }

  parseUserAuth(resp: Response): UserAuth {
    const body = resp.json() || {};
    return new UserAuth(body.access_token, body.expires_in, body.refresh_token)
  }

  parseUserProfile(resp: Response): UserProfile {
    return new UserProfile(resp.json())
  }

  parseErrorMessage(error: Response | any): string {

    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }

    return errMsg
  }

}

export const apiUtils = new ApiUtils();

export function applyMixins(derivedCtor: any, baseCtors: any[]) {
  baseCtors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      derivedCtor.prototype[name] = baseCtor.prototype[name];
    });
  });
}