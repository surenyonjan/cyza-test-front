export class WithFormErrorHandler {

  hasFormError: boolean;
  formErrorMessage: string;

  constructor() {
    this.hasFormError = false;
    this.formErrorMessage = ''
  }

  setFormError(msg: string) {
    this.formErrorMessage = msg;
    this.hasFormError = true
  }

  unsetFormError() {
    this.hasFormError = false;
    this.formErrorMessage = ''
  }
}